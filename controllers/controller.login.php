<?php
$email = $_POST['email'];
$pass  = $_POST['pass'];


include '../resources/class/class.connection.php';
include '../models/model.usuario.php';

try {
    session_start();
    $db->debug();

    $sql = "SELECT * FROM usuarios WHERE Correo=? and Contrasena=?";

    $usuario->get($sql,array($email,$pass));

    if ($usuario->data->Correo==$email) {

        $_SESSION['usuario'] = $usuario->data;

        switch ($usuario->data->IdTipoUsuario) {
            case 1:
                header("location:../?page=alu-inicio");
                break;
            case 2:
                header("location:../?page=pro-inicio");
                break;
            case 3:
                header("location:../?page=adm-inicio");
                break;
        }
    } else {
        $_SESSION['error'] = true;
        header('location:../?page=login');
    }

} catch (Exception $e) {

}
