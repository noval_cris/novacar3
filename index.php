<?php
include "directorios.php";
include 'resources/class/class.connection.php';

//error_reporting(E_ERROR | E_WARNING | E_PARSE);
error_reporting(E_ERROR);

if (isset($_GET["page"])) {
    switch ($_GET['page']) {
        # pagina principal
        case 'nosotros':
            include 'views/home/nosotros.php';
            break;
        case 'contacto':
            include 'views/home/contacto.php';
            break;
        case 'login':
            include 'views/home/login.php';
            break;
        # perfiles
        case 'alu-inicio':
            include 'views/user/alumno/alumnos.php';
            break;
        case 'pro-inicio':
            include 'views/user/profesor/profesor.php';
            break;
        # administrador
        case 'adm-inicio':
            include 'views/user/administrador/inicio.php';
            break;
        case 'adm-materia':
            include 'views/user/administrador/materia.php';
            break;
        case 'adm-materia-editar':
            include 'views/user/administrador/materia_editar.php';
            break;
        case 'adm-usuario':
            include 'views/user/administrador/usuario.php';
            break;
        case 'adm-usuario-editar':
            include 'views/user/administrador/usuario_editar.php';
            break;

    }
} else {
    include 'views/home/inicio.php';
}
