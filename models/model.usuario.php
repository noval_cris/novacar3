<?php
require_once("model.base.php");

class Usuario extends Model {
    public function __construct($db) {
        parent::__construct($db);
        $this->setView ("vw_usuarios");
        $this->setTable("usuarios");

        $this->setKey  ("IdUsuario");
        $this->addField("Nombre");
        $this->addField("ApellidoPat");
        $this->addField("ApellidoMat");
        $this->addField("Correo");
        $this->addField("Contrasena");
        $this->addField("Telefono");
        $this->addField("Domicilio");
        $this->addField("IdTipoUsuario");
    }

    public function getAdministradores() {
        $this->getWhere("IdTipoUsuario=3");
    }

    public function getMaestros() {
        $this->getWhere("IdTipoUsuario=2");
    }

    public function getAlumnos() {
        $this->getWhere("IdTipoUsuario=1");
    }

    public function selectMaestros($value) {
        $this->getMaestros();
        echo "<select name='idmae' id='' class='form-control'>";
        while ($row = $this->next()) {
            if ($row->IdUsuario==$value) $sel = "SELECTED"; else $sel="";
            echo "<option value='$row->IdUsuario' {$sel}>$row->Nombre $row->Apellidos</option>";
        }
        echo "</select>";
    }

    public function selectAlumnos($value) {
        $this->getAlumnos();
        echo "<select name='idalu' id='' class='form-control'>";
        while ($row = $this->next()) {
            if ($row->IdAlumno==$value) $sel = "SELECTED"; else $sel="";
            echo "<option value='$row->IdUsuario' {$sel}>$row->Nombre $row->Apellidos</option>";
        }
        echo "</select>";
    }



}

$usuario = new Usuario($db);
$usuario->newRecord();
?>